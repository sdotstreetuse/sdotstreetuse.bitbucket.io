# README #


### What is this repository for? ###

* Static web wage to implement a 'copy' feature for recording reoccuring events in Seattle Dept of Transportation's  [dotMaps] (https://seattle.dotmapsapp.com/map) right of way management application database
* This repo consists of a single html file with embedded javascript. Using bitbucket pages, this tool is an interim client-side solution to allow city users to duplicate existing events

### Who do I talk to? ###

* Daniel Rockhold of SDOT Street Use Data & GIS